jQuery('.js-hamburger').click(function () {
  jQuery(this).toggleClass('is-active').promise().done(function () {
    if (jQuery(this).hasClass('is-active')) {
      //jQuery('html').addClass('is-overflow');
      jQuery(this).removeClass('not-active');
      jQuery(this).addClass('is-active');
      jQuery('.c-navbar__overlay').addClass('c-navbar__overlay--open');
    } else {
      //jQuery('html').removeClass('is-overflow');
      jQuery(this).removeClass('is-active');
      jQuery(this).addClass('not-active');
      jQuery('.c-navbar__overlay').removeClass('c-navbar__overlay--open');
    }
  });
});


lastScroll = 0;
deltaScroll = 250;
jQuery(window).bind('scroll', function () {
  var scroll = jQuery(window).scrollTop();
  if (scroll > deltaScroll) {
    jQuery('.c-header-fixed').addClass('c-header-fixed--open');
  } else {
    jQuery('.c-header-fixed').removeClass('c-header-fixed--open');
  }
});
