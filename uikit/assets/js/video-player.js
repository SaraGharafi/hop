function videoPlayer() {
  jQuery(function () {
    jQuery( '.o-video-player__video' ).each(function() {
      let video = jQuery(this).get(0);
      let idvideo = jQuery(this).attr('id');
      let parent_video = jQuery(this);
      let play = parent_video.next().find('.o-video-player__controls__icon--play');
      let pause = parent_video.next().find('.o-video-player__controls__icon--pause');
      let stop = parent_video.next().find('.o-video-player__controls__icon--stop');
      let sound = parent_video.next().find('.o-video-player__controls__icon--sound');
      let mute = parent_video.next().find('.o-video-player__controls__icon--mute');

      this.addEventListener('timeupdate', function () {
        updateProgress(this, idvideo)
      }, false);

      // Init Controls
      var attr_autoplay = jQuery(this).attr('autoplay');
      if (typeof attr_autoplay !== typeof undefined && attr_autoplay !== false) {
        play.hide();
      }else{
        pause.hide();
        stop.hide();
      }

      var attr_muted = jQuery(this).attr('muted');
      if (typeof attr_muted !== typeof undefined && attr_muted !== false) {
        mute.hide();
      }else{
        sound.hide();
      }

      // Controls
      // PLAY
      play.click(function () {
        video.play();
        play.hide();
        pause.show();
        stop.show();
      });

      // PAUSE
      pause.click(function () {
        video.pause();
        pause.hide();
        play.show();
        stop.show();
      });

      // STOP
      stop.click(function () {
        video.pause();
        video.currentTime = 0;
        video.play();
        pause.show();
        play.hide();
      });

      // SOUND
      sound.click(function () {
        parent_video.prop('muted', false);
        sound.hide();
        mute.show();
      });

      // MUTE
      mute.click(function () {
        parent_video.prop('muted', true);
        mute.hide();
        sound.show();
      });

    });

    function updateProgress(vid, id) {
      var value = 0;
      if (vid.currentTime > 0) {
        value = (100 / vid.duration) * vid.currentTime;
      }
      jQuery('#o-video-player__progress__line--' + id).width(value + '%');
    }

    // Layer shadow when over video
    /*
    jQuery('.o-video-player__container').mouseenter(function () {
      jQuery(this).children('.o-video-player__controls-container').addClass('o-video-player__controls-container--with-shadow');
    }).mouseleave(function () {
      jQuery(this).children('.o-video-player__controls-container').removeClass('o-video-player__controls-container--with-shadow');
    });
    */

  });
}
videoPlayer();
