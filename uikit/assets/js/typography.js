$('.js-theme-font').each(function () {
  var datafont = $(this).data('font');

  // Family
  var family = $(this).css('font-family');
  var fontweight = $(this).css('font-weight');

  var fontfamily = replaceFamily(family) + ' ('+fontweight+')';
  $('.js-font-family-'+datafont).html(fontfamily);

  // Size
  var size = $(this).css('font-size');
  $('.js-font-size-'+datafont).html(size);

  // Line height
  var lineheight = $(this).css('line-height');
  $('.js-font-line-height-'+datafont).html(lineheight);

  // Color
  var color = $(this).css('color');
  $('.js-font-color-'+datafont).html(color);
})


function replaceFamily(family) {
  family = family.replace(', sans-serif', '');
  family = family.replace(/"/g, '');
  return family;
}

function rgb2hex(rgb) {
  rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
  function hex(x) {
    return ("0" + parseInt(x).toString(16)).slice(-2);
  }
  return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}
