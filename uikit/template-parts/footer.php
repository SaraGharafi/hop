</main>
</div>
<!-- CoreUI and necessary plugins-->
<script src="<?= BASE_URL ?>/assets/vendors/jquery/dist/jquery.min.js"></script>
<script src="<?= BASE_URL ?>/assets/vendors/popper.js/dist/umd/popper.min.js"></script>
<script src="<?= BASE_URL ?>/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?= BASE_URL ?>/assets/vendors/pace-progress/pace.min.js"></script>
<script src="<?= BASE_URL ?>/assets/vendors/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
<script src="<?= BASE_URL ?>/assets/vendors/@coreui/coreui/dist/js/coreui.min.js"></script>
<script src="<?= BASE_URL ?>/assets/vendors/prism/prism.js"></script>
<?php
if (isset($module)): ?>

    <?php if ($module == 'videoplayer'): ?>
    <script src="<?= BASE_URL ?>/assets/js/video-player.js"></script>
    <?php endif; ?>

    <?php if ($module == 'header'): ?>
    <script src="<?= BASE_URL ?>/assets/js/menu.js"></script>
    <?php endif; ?>

    <?php if ($module == 'colors'): ?>
    <script src="<?= BASE_URL ?>/assets/js/colors.js"></script>
    <?php endif; ?>

    <?php if ($module == 'typography'): ?>
    <script src="<?= BASE_URL ?>/assets/js/typography.js"></script>
    <?php endif; ?>

<?php endif; ?>
</body>
</html>
