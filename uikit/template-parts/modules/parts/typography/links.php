<div class="card-header u-uikit__card__title">Links</div>
<?php
foreach ($fonts['links'] as $heading_key => $heading):
    $jskey = 'links-'.$heading_key; ?>
    <div class="card">
        <div class="card-body u-uikit__card-body">
            <div>
                <div class="u-uikit__card-body__title"><?= $heading['heading'] ?></div>
                <div class="u-uikit__card-body__content u-uikit__card-body__content--font">
                    <div class="u-uikit__card-body__content-wrapper u-uikit__card-body__content-wrapper--full">
                        <div class="u-uikit__card-body__content-info">
                            <div class="u-uikit__card-body__content-block">
                                <p class="u-uikit__card-body__content-title">Family</p>
                                <p class="u-uikit__card-body__content-copy js-font-family-<?= $jskey ?>"></p>
                            </div>
                            <div class="u-uikit__card-body__content-block">
                                <p class="u-uikit__card-body__content-title">Size</p>
                                <p class="u-uikit__card-body__content-copy js-font-size-<?= $jskey ?>"></p>
                            </div>
                            <div class="u-uikit__card-body__content-block">
                                <p class=" u-uikit__card-body__content-title">Line Height</p>
                                <p class="u-uikit__card-body__content-copy js-font-line-height-<?= $jskey ?>"></p>
                            </div>
                            <div class="u-uikit__card-body__content-block">
                                <p class=" u-uikit__card-body__content-title">Color</p>
                                <p class="u-uikit__card-body__content-copy js-font-color-<?= $jskey ?>"></p>
                            </div>
                        </div>
                        <div class="u-uikit__card-body__content-info-code">
                            <p class="u-uikit__card-body__content-title">Code</p>
                            <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge"><?= htmlentities($heading['code']) ?></code></p>
                        </div>
                    </div>

                    <div class="u-uikit__card-body__content-text u-uikit__card-body__content-text--font <?php if($heading['type'] == 'light'): ?>u-uikit--bg-grey u-uikit--bg-grey-paddding<?php endif; ?>">
                        <a href="#" class="<?= $heading['code'] ?> js-theme-font" data-font="<?= $jskey ?>"><?= $example_texts[array_rand($example_texts)];?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
