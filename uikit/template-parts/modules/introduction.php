<?php include(__DIR__ . '/../../template-parts/head.php'); ?>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="u-wrapper">
            <div class="u-uikit__intro">
                <div class="u-grid grid-gap--xxs">
                    <div class="col">
                        <h1>Pixel Division UI KIT</h1>
                    </div>
                </div>

                <div class="u-uikit__intro__section">
                    <div class="u-grid grid-gap--xxs">
                        <div class="col">
                            <h2 class="u-uikit__intro__title">Getting started</h2>

                            <div class="u-uikit__intro__content">
                                Area de introducción y documentación
                            </div>
                        </div>
                    </div>
                </div>

                <div class="u-uikit__intro__section">
                    <div class="u-grid grid-gap--xxs">
                        <div class="col-5 col-12@desktop-wide">
                            <h2 class="u-uikit__intro__title">Instalación</h2>

                            <div class="u-uikit__intro__content">
                                Para empezar ha utilizar el UIKIT hay que bajarse el repositiorio de Bitbucket. Si vas a empezar con un proyecto SAGE ya está incluído en el repositiorio, en la carpeta 'uikit'. Para bajar sólo el uikit usa el siguiente comando.
                            </div>
                        </div>

                        <div class="col-7 col-12@desktop-wide">

                            <div class="u-grid grid-gap--xxs">
                                <div class="col">

                                    <div class="u-uikit__card-body__content-code">
                                        <pre><code class="language-git">git clone git@bitbucket.org:pixeldivisiondev/base-ui-kit.git </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="u-uikit__intro__section">
                    <div class="u-grid grid-gap--xxs">
                        <div class="col-5 col-12@desktop-wide">
                            <h2 class="u-uikit__intro__title">Instrucciones</h2>

                            <div class="u-uikit__intro__content">
                                Dentro de la carpeta <i><b>config</b></i> encontraremos los ficheros config que tenemos que modificar.
                            </div>
                        </div>

                        <div class="col-7 col-12@desktop-wide">

                            <div class="u-grid grid-gap--xxs">
                                <div class="col">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="u-uikit__intro__section">
                    <div class="u-grid grid-gap--xxs">
                        <div class="col-5 col-12@desktop-wide">
                            <h2 class="u-uikit__intro__title">Theme config</h2>

                            <div class="u-uikit__intro__content">
                                En el fichero <i><b>theme.php</b></i> tenemos que modificar la configuración general del UIKIT. Por ejemplo en la variable $path_theme_css tenemos que poner la ruta de nuestro main.css de la web.
                            </div>
                        </div>
                        <div class="col-7 col-12@desktop-wide">

                            <div class="u-grid grid-gap--xxs">
                                <div class="col">
                                    <div class="u-uikit__card-body__content-code">
                                        <pre><code class="language-php">/*
 * CSS main de la web
 */
$path_theme_css = BASE_WEB . '/dist/styles/main.css';</code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="u-uikit__intro__section">
                    <div class="u-grid grid-gap--xxs">
                        <div class="col-5 col-12@desktop-wide">
                            <h2 class="u-uikit__intro__title">Menu</h2>

                            <div class="u-uikit__intro__content">
                                En el fichero <i><b>menu.php</b></i> vamos creando el menú de la izquierda mediante arrays. La key del array es el nombre del fichero y el valor es el nombre del menú.
                            </div>
                        </div>
                        <div class="col-7 col-12@desktop-wide">

                            <div class="u-grid grid-gap--xxs">
                                <div class="col">
                                    <div class="u-uikit__card-body__content-code">
                                        <pre><code class="language-php">$menu['Overview'] = array(
    'introduction' => 'Introduction',
);

$menu['Style'] = array(
    'colors' => 'Colors',
    'typography' => 'Typography',
    'layout' => 'Layout',
    'icons' => 'Icons',
);</code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="u-uikit__intro__section">
                    <div class="u-grid grid-gap--xxs">
                        <div class="col-5 col-12@desktop-wide">
                            <h2 class="u-uikit__intro__title">Layout</h2>

                            <div class="u-uikit__intro__content">
                                En el fichero <i><b>layout.php</b></i> hay que ir rellenando los arrays con la configuración de grid, wrappers y media queries de nuestra web.
                            </div>
                        </div>
                        <div class="col-7 col-12@desktop-wide">

                            <div class="u-grid grid-gap--xxs">
                                <div class="col">
                                    <div class="u-uikit__card-body__content-code">
                                        <pre><code class="language-php">$layouts = array(
    'grid' => array(
        array('name' => 'Layout desktop', 'width' => '1440px'),
        array('name' => 'Layout desktop', 'width' => '1110px'),
    ),
    'wrappers' => array(
        array('name' => 'Wrapper', 'class' => 'u_wrapper', 'width' => '1440px', 'variable' => '--wrapper-size'),
        array('name' => 'Wrapper narrow', 'class' => 'u_wrapper u_wrapper_narrow', 'width' => '1110px', 'variable' => '--wrapper-size-narrow'),
    ),
    'spacedefault' => 8,
    'spacing' => array(
        array('name' => 'padding-default', 'attribute' => 'padding', 'value' => '48px', 'variable' => '--padding-default'),
    ),
    'breakpoints' => array(
        array('name' => 'desktop-wide',  'value' => '1440px'),
        array('name' => 'desktop',  'value' => '1248px'),
        array('name' => 'tablet-wide',  'value' => '1024px'),
        array('name' => 'tablet-small',  'value' => '768px'),
        array('name' => 'phablet',  'value' => '640px'),
        array('name' => 'mobile-wide',  'value' => '480px'),
        array('name' => 'mobile',  'value' => '400px'),
    ),
);</code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="u-uikit__intro__section">
                    <div class="u-grid grid-gap--xxs">
                        <div class="col-5 col-12@desktop-wide">
                            <h2 class="u-uikit__intro__title">Colors</h2>

                            <div class="u-uikit__intro__content">
                                En el fichero <i><b>colors.php</b></i> hay que ir rellenando los arrays según nuestas variables creadas en colors de nuestro sass de la web.
                            </div>
                        </div>
                        <div class="col-7 col-12@desktop-wide">

                            <div class="u-grid grid-gap--xxs">
                                <div class="col">
                                    <div class="u-uikit__card-body__content-code">
                                        <pre><code class="language-php">$colors = array(
    'theme' => array(
        '--color-primary',
        '--color-secondary'
    ),
    'grayscale' => array(
        '--color-white',
        '--color-gray-100',
        '--color-gray-200',
        '--color-gray-300',
        '--color-gray-400',
        '--color-gray-500',
        '--color-gray-600',
        '--color-gray-700',
        '--color-gray-800',
        '--color-gray-900',
        '--color-black'
    ),
    'additional' => array(
        '--color-success',
        '--color-error',
        '--color-warning',
        '--color-info'
    )
);</code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="u-uikit__intro__section">
                    <div class="u-grid grid-gap--xxs">
                        <div class="col-5 col-12@desktop-wide">
                            <h2 class="u-uikit__intro__title">Typography</h2>

                            <div class="u-uikit__intro__content">
                                En el fichero <i><b>fonts.php</b></i> hay que ir rellenando los arrays según nuestas variables creadas en fonts de nuestro sass de la web.
                            </div>
                        </div>
                        <div class="col-7 col-12@desktop-wide">

                            <div class="u-grid grid-gap--xxs">
                                <div class="col">
                                    <div class="u-uikit__card-body__content-code">
                                        <pre><code class="language-php">$fonts = array(
    'theme_fonts' => array(
        array('type' => 'primary', 'name' => 'Open Sans', 'weight' => 'bold'),
        array('type' => 'secondary', 'name' => 'Open Sans', 'weight' => 'regular'),
        array('type' => 'tertiary', 'name' => 'Open Sans', 'weight' => 'regular'),
    ),
    'font_usage' => array(
        array('type' => 'primary', 'name' => 'Open Sans', 'weight' => 'bold'),
        array('type' => 'secondary', 'name' => 'Open Sans', 'weight' => 'regular'),
        array('type' => 'secondary', 'name' => 'Open Sans', 'weight' => 'medium'),
        array('type' => 'secondary', 'name' => 'Open Sans', 'weight' => 'bold'),
        array('type' => 'tertiary', 'name' => 'Open Sans', 'weight' => 'regular'),
        array('type' => 'tertiary', 'name' => 'Open Sans', 'weight' => 'bold'),
    ),
    'text' => array(
        array('heading' => 'Text', 'code' => 'u-text', 'type' => 'dark'),
    ),
    'paragraphs' => array(
        array('heading' => 'Paragraph', 'code' => 'u-text', 'type' => 'dark'),
    ),
    'links' => array(
        array('heading' => 'Link', 'code' => 'u-text', 'type' => 'dark'),
    ),
    'lists' => array(
        array('heading' => 'List', 'code' => 'u-text', 'type' => 'dark'),
    ),
);</code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="u-uikit__intro__section">
                    <div class="u-grid grid-gap--xxs">
                        <div class="col-5 col-12@desktop-wide">
                            <h2 class="u-uikit__intro__title">Butttons</h2>

                            <div class="u-uikit__intro__content">
                                En el fichero <i><b>buttons.php</b></i> hay que ir rellenando los arrays según nuestas variables creadas en buttons de nuestro sass de la web.
                            </div>
                        </div>
                        <div class="col-7 col-12@desktop-wide">

                            <div class="u-grid grid-gap--xxs">
                                <div class="col">
                                    <div class="u-uikit__card-body__content-code">
                                        <pre><code class="language-php">$buttons = array(
    'theme_buttons' => array(
        array('heading' => 'Default', 'code' => 'o-button'),
        array('heading' => 'Button primary', 'code' => 'o-button o-button--color-primary'),
        array('heading' => 'Button secondary', 'code' => 'o-button o-button--color-secondary'),
        array('heading' => 'Button Outline', 'code' => 'o-button o-button--outline'),
        array('heading' => 'Button Size small', 'code' => 'o-button o-button--color-primary o-button--size-sm'),
        array('heading' => 'Button Size Medium', 'code' => 'o-button o-button--color-primary o-button--size-md'),
        array('heading' => 'Button Size Big', 'code' => 'o-button o-button--color-primary o-button--size-lg'),
        array('heading' => 'Button Size Full', 'code' => 'o-button o-button--color-primary o-button--size-full'),
    ),
);</code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="u-uikit__intro__section">
                    <div class="u-grid grid-gap--xxs">
                        <div class="col-5 col-12@desktop-wide">
                            <h2 class="u-uikit__intro__title">Icons</h2>

                            <div class="u-uikit__intro__content">
                                En el fichero <i><b>icons.php</b></i> hay que ir rellenando los arrays con los logos y modificar la URL de nuestro scss con los iconos.
                            </div>
                        </div>
                        <div class="col-7 col-12@desktop-wide">

                            <div class="u-grid grid-gap--xxs">
                                <div class="col">
                                    <div class="u-uikit__card-body__content-code">
                                        <pre><code class="language-php">$path_theme_icons = SAGE_PATH . '/resources/assets/styles/utilities/_utilities.icons.scss';

$icons = array(
    'logos' => array(
        array('name' => 'default', 'url' => BASE_WEB .'/resources/assets/images/logo.svg', 'type' => 'dark'),
        array('name' => 'dark', 'url' => BASE_URL .'/assets/img/brand/logo-habitant-dark.jpg', 'type' => 'light'),
        array('name' => 'light', 'url' => BASE_URL .'/assets/img/brand/logo-habitant-light.jpg', 'type' => 'light'),
    ),
);</code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="u-uikit__intro__section">
                    <div class="u-grid grid-gap--xxs">
                        <div class="col-5 col-12@desktop-wide">
                            <h2 class="u-uikit__intro__title">Surface</h2>

                            <div class="u-uikit__intro__content">
                                En el fichero <i><b>surface.php</b></i>  hay que ir rellenando los arrays según nuestas variables creadas en surface de nuestro sass de la web.
                            </div>
                        </div>
                        <div class="col-7 col-12@desktop-wide">

                            <div class="u-grid grid-gap--xxs">
                                <div class="col">
                                    <div class="u-uikit__card-body__content-code">
                                        <pre><code class="language-php">$surfaces = array(
    'theme_surfaces' => array(
        array('heading' => 'Color white', 'code' => 'u-surface u-surface-–color-white'),
        array('heading' => 'Color primary', 'code' => 'u-surface u-surface--color-primary'),
        array('heading' => 'Color secondary', 'code' => 'u-surface u-surface--color-secondary'),
        array('heading' => 'Color Gray 100', 'code' => 'u-surface u-surface--color-gray-100'),
    ),
);</code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="u-uikit__intro__section">
                    <div class="u-grid grid-gap--xxs">
                        <div class="col-5 col-12@desktop-wide">
                            <h2 class="u-uikit__intro__title">Footer</h2>

                            <div class="u-uikit__intro__content">
                                En el fichero <i><b>template-parts/footer.php</b></i> vamos añadiendo según el modulo (key del array del menu) ficheros js..
                            </div>
                        </div>
                        <div class="col-7 col-12@desktop-wide">

                            <div class="u-grid grid-gap--xxs">
                                <div class="col">
                                    <div class="u-uikit__card-body__content-code">
                                        <pre><code class="language-php"><?php echo htmlentities('<?php if ($module == "colors"): ?>
    <script src="assets/js/colors.js"></script>
<?php endif; ?>

<?php if ($module == "typography"): ?>
    <script src="assets/js/typography.js"></script>
<?php endif; ?>');?></code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="u-uikit__intro__section">
                    <div class="u-grid grid-gap--xxs">
                        <div class="col-5 col-12@desktop-wide">
                            <h2 class="u-uikit__intro__title">Módulos</h2>

                            <div class="u-uikit__intro__content">
                                Dentro de template-parts/modules/ vamos ha encontrar los distintos módulos de nuestro UIKIT. Si queremos crear uno nuevo, duplicamos el fichero <i><b>default.php</b></i> y vamos rellenando con el componente que vamos a crear.
                            </div>
                        </div>
                        <div class="col-7 col-12@desktop-wide">

                            <div class="u-grid grid-gap--xxs">
                                <div class="col">
                                    <div class="u-uikit__card-body__content-code">
                                        <pre><code class="language-html"><?php echo htmlentities('<div class="u-uikit__card-body__content-text">

<!-- Aqui va el código -->

</div>');?></code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php include(__DIR__ . '/../../template-parts/footer.php'); ?>




