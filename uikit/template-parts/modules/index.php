<?php include(__DIR__ . '/../../template-parts/head.php'); ?>

<div class="container-fluid u-uikit--height100">
    <div class="animated fadeIn u-uikit--height100">
        <div class="u-uikit__home u-uikit--height100">
            <p class="u-uikit__home__title">UI STUDIO</p>
            <p class="u-uikit__home__subtitle">BETA</p>
        </div>
    </div>
</div>

<?php include(__DIR__ . '/../../template-parts/footer.php'); ?>
