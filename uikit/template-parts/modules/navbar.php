<?php include(__DIR__ . '/../../template-parts/head.php'); ?>
<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="u-wrapper">
      <div class="u-uikit__header u-wrapper u-wrapper--narrow">
        <p class="u-uikit__header__pretitle">Navigation</p>
        <p class="u-uikit__header__title">Topbar / Menu</p>
      </div>

      <div class="card">
        <div class="card-body u-uikit__card-body">
          <div>
            <div class="u-uikit__card-body__title">Default</div>
            <div class="u-uikit__card-body__content">
              <div class="u-uikit__card-body__content-wrapper u-uikit__card-body__content-wrapper--full">
                <div>
                  <p class="u-uikit__card-body__content-title">Code</p>
                  <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge">c-navbar</code></p>
                </div>
              </div>
              <div class="u-uikit__card-body__content-text u-uikit--bg-dark">
                <ul id="menu" class="c-navbar">
                  <li class="c-navbar__item"><a class="c-navbar__item__link" href="#">Nosotros</a></li>
                  <li class="c-navbar__item"><a class="c-navbar__item__link" href="#">Servicios</a></li>
                  <li class="c-navbar__item"><a class="c-navbar__item__link" href="#">Trabajos</a></li>
                  <li class="c-navbar__item"><a class="c-navbar__item__link" href="#">Careers</a></li>
                  <li class="c-navbar__item"><a class="c-navbar__item__link" href="#">Blog</a></li>
                  <li class="c-navbar__item"><a class="c-navbar__item__link" href="#">El Garaje</a></li>
                  <li class="c-navbar__item"><a class="c-navbar__item__link" href="#">Contacto</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>
</div>
<?php include(__DIR__ . '/../../template-parts/footer.php'); ?>
