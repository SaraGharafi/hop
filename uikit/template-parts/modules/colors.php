<?php
include(__DIR__ . '/../../template-parts/head.php');
$module = 'colors';
?>
<div class="container-fluid u-uikit__colors">
    <div class="animated fadeIn">
        <div class="u-wrapper u-wrapper--narrow">
            <div class="u-uikit__header">
                <p class="u-uikit__header__pretitle">Style</p>
                <p class="u-uikit__header__title">Colors</p>
            </div>
            <?php
            foreach ($colors as $key => $theme_colors): ?>
                <div class="card">
                    <div class="card-header"><?= $key; ?> colors</div>
                    <div class="card-body">
                        <div class="u-uikit__colors__text-colors">
                            <?php
                            foreach ($theme_colors as $color_key): $jskey = $key.'-colors-' . $color_key; ?>
                                <div class="u-uikit__colors__block">
                                    <div class="u-uikit__colors__block__color js-theme-color" data-color="<?= $jskey ?>" style="background-color: var(<?= $color_key; ?>)"></div>
                                    <p class="u-uikit__colors__block__title"><?= $color_key; ?></p>
                                    <p class="u-uikit__colors__block__hex js-color-hex-<?= $jskey ?>"></p>
                                    <p class="u-uikit__colors__block__rgb js-color-rgb-<?= $jskey ?>"></p>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="u-uikit__card-body__content-code">
                            <pre><code class="language-css"><?php foreach ($theme_colors as $color_key): ?>var(<?= $color_key . ')<br>'; ?><?php endforeach; ?></code></pre>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<?php include(__DIR__ . '/../../template-parts/footer.php'); ?>
