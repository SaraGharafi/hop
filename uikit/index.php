<?php
include('template-parts/head.php');
$module = 'index'; ?>
<div class="container-fluid u-uikit--height100">
    <div class="animated fadeIn u-uikit--height100">
        <div class="u-uikit__home u-uikit--height100">
            <div class="u-uikit__home__intro">
                <h1 class="u-uikit__home__title">UI KIT<sup>®</sup></h1>

                <p class="u-uikit__home__subtitle">
                    Pixel Division new flexible and modular design system for design and development teams.
                </p>
            </div>

            <div>
                <img src="<?= BASE_URL ?>/assets/img/brand/home-img.png" alt="">
            </div>
        </div>
    </div>
</div>
<?php include('template-parts/footer.php'); ?>
