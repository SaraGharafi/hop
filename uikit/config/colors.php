<?php

$colors = array(
    'theme' => array(
        '--color-primary',
        '--color-secondary'
    ),
    'secondary' => array(
        '--color-white',
        '--color-black',
    ),
    'gold' => array(
        '--color-gold',
        '--color-gold-200',
        '--color-gold-300',
        '--color-gold-400',
        '--color-gold-600',
        '--color-gold-700',
        '--color-gold-800',
        '--color-gold-900',
    ),
    'grayscale' => array(
        '--color-white',
        '--color-gray-100',
        '--color-gray-200',
        '--color-gray-300',
        '--color-gray-400',
        '--color-gray-500',
        '--color-gray-600',
        '--color-gray-700',
        '--color-gray-800',
        '--color-gray-900',
        '--color-black',

    ),
    'additional' => array(
        '--color-success',
        '--color-error',
        '--color-warning',
        '--color-info'
    )
);

?>
