<?php


$buttons = array(
    'theme_buttons' => array(
        array('heading' => 'Default', 'code' => 'o-button o-button--color-primary'),
        array('heading' => 'Button primary', 'code' => 'o-button o-button--color-transparent'),
        array('heading' => 'Button secondary', 'code' => 'o-button o-button--color-secondary'),
        array('heading' => 'Button Outline', 'code' => 'o-button o-button--outline'),


        array('heading' => 'Button Size small', 'code' => 'o-button o-button--color-primary o-button--size-sm'),
        array('heading' => 'Button Size Medium', 'code' => 'o-button o-button--color-primary o-button--size-md'),
        array('heading' => 'Button Size Big', 'code' => 'o-button o-button--color-primary o-button--size-lg'),
        array('heading' => 'Button Size Full', 'code' => 'o-button o-button--color-primary o-button--size-full'),
    ),
);

?>
