<?php

$path_theme_icons = SAGE_PATH . '/resources/assets/styles/utilities/_utilities.icons.scss';

$icons = array(
    'logos' => array(
        array('name' => 'default', 'url' => BASE_WEB .'/resources/assets/images/logos/logo-dark.svg', 'type' => 'light'),
        array('name' => 'light', 'url' => BASE_WEB .'/resources/assets/images/logos/logo-light.svg', 'type' => 'dark'),
        array('name' => 'darkNoTagline', 'url' => BASE_WEB .'/resources/assets/images/logos/logo-darkNoTagline.svg', 'type' => 'light'),
        array('name' => 'lightNoTagline', 'url' => BASE_WEB .'/resources/assets/images/logos/logo-lightNoTagline.svg', 'type' => 'light'),
    ),
);
?>
