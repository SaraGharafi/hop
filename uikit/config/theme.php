<?php

/*
 * Configuración global
 */
define('SAGE_PATH',  $_SERVER["DOCUMENT_ROOT"].'/wp-content/themes/hop');
define('ROOT_PATH',  $_SERVER["DOCUMENT_ROOT"].'/wp-content/themes/hop/uikit');
define('BASE_URL', 'http://hop.test/wp-content/themes/hop/uikit');
define('BASE_WEB', 'http://hop.test/wp-content/themes/hop/');
define('BASE_NAME', 'Pixel Division UIKIT');

/*
 * CSS main de la web
 */
$path_theme_css = BASE_WEB . '/dist/styles/main.css';

/*
 * Textos de ejemplos
 */
$example_texts = array(
    'The quick brown fox jumps over the lazy dog',
    'The five boxing wizards jump quickly',
    'The wizard quickly jinxed the gnomes before they vaporized'
);

/*
 * Include Files
 */

// MENU
include_once(ROOT_PATH .'/config/menu.php');

// COLORS
include_once(ROOT_PATH .'/config/colors.php');

// FONTS
include_once(ROOT_PATH .'/config/fonts.php');

// LAYOUT
include_once(ROOT_PATH .'/config/layout.php');

// ICONS
include_once(ROOT_PATH .'/config/icons.php');

// BUTTONS
include_once(ROOT_PATH .'/config/buttons.php');

// SURFACE
include_once(ROOT_PATH .'/config/surface.php');

// CARDS
include_once(ROOT_PATH .'/config/cards.php');
