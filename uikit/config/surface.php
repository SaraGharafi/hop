<?php

$surfaces = array(
    'theme_surfaces' => array(
        array('heading' => 'Color white', 'code' => 'u-surface u-surface-–color-white'),
        array('heading' => 'Color primary', 'code' => 'u-surface u-surface--color-primary'),
        array('heading' => 'Color secondary', 'code' => 'u-surface u-surface--color-secondary'),
        //array('heading' => 'Color gold', 'code' => 'u-surface u-surface--color-gold'),
        //por que no puedo agragar otro color?
        array('heading' => 'Color Gray 100', 'code' => 'u-surface u-surface--color-gray-100'),
    ),
);

?>
