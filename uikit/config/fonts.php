<?php

$fonts = array(
    'theme_fonts' => array(
        array('type' => 'primary', 'name' => 'Poppins', 'weight' => 'regular'),
        array('type' => 'secondary', 'name' => 'Roboto', 'weight' => 'regular'),
    ),
    'font_usage' => array(
        array('type' => 'primary', 'name' => 'Poppins', 'weight' => 'bold'),
        array('type' => 'secondary', 'name' => 'Roboto', 'weight' => 'medium'),
        array('type' => 'secondary', 'name' => 'Roboto', 'weight' => 'regular'),
        array('type' => 'secondary', 'name' => 'Roboto', 'weight' => 'light'),
    ),
    'text' => array(
        array('heading' => 'Text', 'code' => 'u-text', 'type' => 'dark'),
        array('heading' => 'Display1', 'code' => 'u-text--display1', 'type' => 'dark'),
        array('heading' => 'Display2', 'code' => 'u-text--display2', 'type' => 'dark'),
        array('heading' => 'Display3', 'code' => 'u-text--display3', 'type' => 'dark'),
        array('heading' => 'Display4', 'code' => 'u-text--display4', 'type' => 'dark'),
    ),
    'paragraphs' => array(
        array('heading' => 'Paragraph', 'code' => 'u-text', 'type' => 'dark'),
    ),
    'links' => array(
        array('heading' => 'Link', 'code' => 'u-text', 'type' => 'dark'),
    ),
    'lists' => array(
        array('heading' => 'List', 'code' => 'u-text', 'type' => 'dark'),
    ),
);

?>
