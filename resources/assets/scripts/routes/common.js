import '../modules/video-player';

export default {
  init() {
  },
  // JavaScript to be fired on all pages
finalize() {
  // JavaScript to be fired on all pages, after page specific JS is fired
  $('#videoPlayer').pixelplayer({
    controls: true,
    center: true,
    progress: true,
    functions: {
      sound: true,
      mute: true,
    },
    overlay: true,
  });
},
};
