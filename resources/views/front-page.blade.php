@extends('layouts.app')

@section('content')
  @include('partials.components.hero')
  @include('partials.components.text-flex')
  @include('partials.components.proyectos')
  @include('partials.components.flex')
@endsection
