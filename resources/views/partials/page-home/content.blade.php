<section class="o-section">
  <div class="u-wrapper">
    <div class="u-grid grid-gap--lg">
      <div class="col col-12@mobile-wide u-flex u-flex-column">
        <span class="o-section__pretitle">Librería de componentes web</span>
        <h1 class="o-section__title">Antes de empezar a maquetar módulos ...</h1>

        <div class="o-section__copy">
          <p>Revisa la <strong>librería de componentes</strong> de nuestro Design System porque puede que el módulo que necesitas programar ya esté preparado para simplemente copiar y pegarlo. </p>
        </div>

        <a href="http://pre7.pixeldivision.es/design-system/componentes/" target="_blank" class="o-section__cta o-button o-button--color-primary">Explorar componentes</a>
      </div>

      <div class="col col-12@mobile-wide ">
        <img src="@asset('images/componentes-web.png')" alt="">
      </div>
    </div>
  </div>
</section>
