<div class="c-hero">

  <div class="c-hero__container">
    <div class="c-hero__container__text">
      <span class="c-hero__pretitle">premios thehop</span>
      <h1 class="c-hero__title">
        Es hora de dar un salto <span class="c-hero__plus">+</span> allá
      </h1>

      <div class="c-hero__description">
        <p>
          <strong>TheHop, el Programa de Emprendimiento Colaborativo de Estrella Galicia</strong> sigue evolucionando
          para conectar el mejor talento externo con nuestra marca. Si tienes una startup tecnológico-digital, éste es
          nuestro momento. <strong>Saltemos juntos. Saltemos + lejos.</strong>
        </p>
      </div>

      <a href="#" class="c-hero__cta o-button o-button--color-transparent" target="_blank">
        Descubre el programa
        <i class="u-icon--arrow-right "></i>
      </a>

      <div class="c-hero__pretitle--final">Empieza ahora</div>

    </div>

    <div class="c-hero__container__video" id="videoPlayer" data-overlay="@asset('images/poster-video/the-hop.png')">
      <video class="o-video-player__video">
        <source src="https://thehop.xyz/wp-content/uploads/2019/05/THEHOP_VIDEOLOOP.mp4" type="video/mp4">
        <source src="https://thehop.xyz/wp-content/uploads/2019/05/THEHOP_VIDEOLOOP.webm" type="video/webm">
        Sorry, your browser doesn't support HTML5 video.
      </video>
    </div>

  </div>

</div>
