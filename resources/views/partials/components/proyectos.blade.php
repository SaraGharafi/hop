<section class="c-box-article">
  <div class="c-box-article__container u-wrapper">


    <div class="c-box-article__container__title u-padding--right-lg u-padding--bottom-xl">
      <h2 class="c-box-article__container__title__titulo--bigger">4 puntos de apoyo para tomar <span
          class="c-hero__plus--small">+</span> impulso</h2>
    </div>


    <div class="c-box-article__container__text u-margin--bottom-xxl">
      <p class="u-margin--right-md ">
        Al menos tienes <strong>cuatro motivos para querer que tu startup sea seleccionada</strong> en la segunda
        edición del Programa TheHop.
      </p>
    </div>


    <div class="o-card">

      <div class="o-card__item">

        <div class="o-card__img">
          <img src="@asset('images/card/unsplash.png')" alt="">
        </div>

        <div class="o-card__texto">
          <div class="o-card__title u-margin--bottom-sm">1. Crearás un proyecto piloto con Estrella Galicia</div>
          <div class="o-card__txt">
            <p>Y eso no solo significa poder materializar tu idea con una gran compañía, sino la posibilidad de
              establecer una relación a largo plazo con nosotros.</p>
          </div>
        </div>

      </div>


      <div class="o-card__item">

        <div class="o-card__img">
          <img src="@asset('images/card/unsplash2.png')" alt="">
        </div>

        <div class="o-card__texto">
          <div class="o-card__title u-margin--bottom-sm">2. Te rodearás de los mejores en un espaZo único</div>
          <div class="o-card__txt">
            <p>Trabajarás en el desarrollo de tu piloto en un espacio creado específicamente para el programa, en el que accederás a una red de contactos y oportunidades de desarrollo que te permitirán diferenciarte en el mercado.</p>
          </div>
        </div>

      </div>


      <div class="o-card__item">

        <div class="o-card__img">
          <img src="@asset('images/card/unsplash3.jpeg')" alt="">
        </div>

        <div class="o-card__texto">
          <div class="o-card__title u-margin--bottom-sm">3. Recibirás apoyo personalizado</div>
          <div class="o-card__txt">
            <p>Para desarrollar tu proyecto contarás con un plan personalizado en el que expertos de distintas disciplinas te ofrecerán formación y tutorías experienciales.</p>
          </div>
        </div>

      </div>


      <div class="o-card__item">

        <div class="o-card__img">
          <img src="@asset('images/card/unsplash4.jpg')" alt="">
        </div>

        <div class="o-card__texto">
          <div class="o-card__title u-margin--bottom-sm">4. Y la mentorización de nuestros directivos</div>
          <div class="o-card__txt">
            <p>TheHop es una prioridad para Estrella Galicia, y por eso mentores corporativos y directivos de la compañía estarán cerca de ti durante todo el programa.</p>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
