<section class="c-box-article c-box-article--bg">
  <div class="c-box-article__container u-wrapper">

    <div class="c-box-article__container__title u-padding--right-lg">
      <h2 class="c-box-article__container__title__titulo--bigger">Hagamos
        <span class="c-hero__plus--small">+</span> fuerte
        nuestra cadena de valor
      </h2>
    </div>

    <div class="c-box-article__container__text u-margin--bottom-xxl">
      <p class="u-margin--right-md ">
        Para participar en TheHop, tu startup debe tener un producto o modelo de negocio lo suficientemente avanzado como para poder desarrollar con nosotros <strong>un proyecto piloto que refuerce una de las áreas de nuestra cadena de valor:</strong>
      </p>
    </div>

    <div class="o-icon">

      <div class="o-icon__item">

        <div>
          <img src="@asset('images/icons/industria.svg')" alt="">
        </div>

        <div class="o-icon__title">
          Industry 4.0
        </div>

      </div>
      <div class="o-icon__item">

        <div>
          <img src="@asset('images/icons/distribucion.svg')" alt="">
        </div>

        <div class="o-icon__title">
          Distribucion Channels
        </div>

      </div>
      <div class="o-icon__item">

        <div>
          <img src="@asset('images/icons/digital.svg')" alt="">
        </div>

        <div class="o-icon__title">
          Go todigital market
        </div>

      </div>
      <div class="o-icon__item">

        <div>
          <img src="@asset('images/icons/impacto.svg')" alt="">
        </div>

        <div class="o-icon__title">
          Impacto social
        </div>

      </div>

    </div>
  </div>
</section>
