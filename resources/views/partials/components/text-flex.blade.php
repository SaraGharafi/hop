<section class="c-box-article">
  <div class="c-box-article__container u-wrapper">


    <div class="c-box-article__container__title">
      <h2 class="c-box-article__container__title__titulo u-margin--bottom-md">Saltemos juntos
        Saltemos <span class="c-hero__plus--small">+</span> lejos</h2>
      <p class="c-box-article__container__title__sub">En 2019 TheHop vuelve con</p>
      <p class="c-box-article__container__title__sub">
        <span class="c-hero__plus--smaller">+</span> fuerza,</p>
      <p class="c-box-article__container__title__sub">
        <span class="c-hero__plus--smaller">+</span> ambición,</p>
      <p class="c-box-article__container__title__sub">
        <span class="c-hero__plus--smaller">+</span> oportunidades.</p>
    </div>


    <div class="c-box-article__container__text u-margin--bottom-xxl">
      <p class="u-margin--bottom-sm">Gracias a las startups que quisieron dar el salto con nosotros, la primera edición de TheHop ha sido un gran éxito. Tanto, que en nuestra segunda edición queremos dar un salto más allá: Buscamos a 6 startups en plena fase de crecimiento, con capacidad para crear un proyecto piloto <strong>financiado hasta 45.000 euros por Estrella Galicia</strong> y desarrollado dentro de nuestro ecosistema.
      </p>

      <p class="u-margin--bottom-sm"><strong>Queremos seguir siendo la cerveza + querida</strong>, y para ello nos gustaría contar con tu talento:
      </p>

      <p>Si tienes <strong>un modelo de innovación aplicable a la industria de bebidas</strong> y cuentas con un proyecto que nos haga crecer juntos y que tenga un claro impacto social y generen beneficio para las personas, súmate al Programa TheHop.
      </p>
    </div>


    <div class="c-box-article__container__img">
      <img  src="@asset('images/home/conferencia.jpg')" alt="Conferencia-theHop">
    </div>
  </div>
</section>
