<header class="c-header c-header--fixed">
  <div class="c-header__container">
    <a class="c-header__container__logo" href="#"><img src="@asset('images/logos/logo-dark.svg')" alt="The Hop"></a>

    <div class="u-hidden u-show@tablet">
      <div class="c-hamburger js-hamburger js-menu-toggle">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>

    <nav class="c-menu js-menu">
      <a class="c-menu__item active" href="#">Qué es TheHop</a>
      <a class="c-menu__item" href="#">Ediciones anteriores</a>
      <a class="c-menu__item" href="#">Estrella Galicia</a>
      <a class="c-menu__item" href="#">Comunidad</a>
      <a class="c-menu__item c-menu__item--button" href="#">Participa</a>

    </nav>
  </div>
</header>
