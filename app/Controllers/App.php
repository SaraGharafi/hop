<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use Mobile_Detect;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public function opcionesGenerales()
    {
        global $wpdb;
        $data = array();
        $results = $wpdb->get_results("SELECT option_name FROM `" . $wpdb->prefix . "options` WHERE `option_name` LIKE 'options_%'");
        foreach ($results as $result):
            $trimmed = str_replace('options_', '', $result->option_name);
            $data[$trimmed] = get_field($trimmed, 'option');
        endforeach;
        return $data;
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    // Any mobile device (phones or tablets).
    public function isMobile()
    {
        $detect = new Mobile_Detect;
        return $detect->isMobile();
    }

    // Any tablet device.
    public function isTablet()
    {
        $detect = new Mobile_Detect;
        return $detect->isTablet();
    }

    // Check for a specific platform with the help of the magic methods:
    public function isiOS()
    {
        $detect = new Mobile_Detect;
        return $detect->isiOS();
    }

    // Check for a specific platform with the help of the magic methods:
    public function isAndroidOS()
    {
        $detect = new Mobile_Detect;
        return $detect->isAndroidOS();
    }
}
