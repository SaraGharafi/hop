<?php

use function App\sage;

add_action('after_setup_theme', function () {


    sage('blade')->compiler()->directive('desktop', function ($expression) {
        return "<?php if(!App::isMobile()){ ?>";
    });

    sage('blade')->compiler()->directive('enddesktop', function ($expression) {
        return "<?php } ?>";
    });

    sage('blade')->compiler()->directive('mobile', function ($expression) {
        return "<?php if(App::isMobile()){ ?>";
    });

    sage('blade')->compiler()->directive('endmobile', function ($expression) {
        return "<?php } ?>";
    });

    sage('blade')->compiler()->directive('tablet', function ($expression) {
        return "<?php if(App::isTablet()){ ?>";
    });

    sage('blade')->compiler()->directive('endtablet', function ($expression) {
        return "<?php } ?>";
    });

    sage('blade')->compiler()->directive('ios', function ($expression) {
        return "<?php if(App::isiOS()){ ?>";
    });

    sage('blade')->compiler()->directive('endios', function ($expression) {
        return "<?php } ?>";
    });

    sage('blade')->compiler()->directive('android', function ($expression) {
        return "<?php if(App::isAndroidOS()){ ?>";
    });

    sage('blade')->compiler()->directive('endandroid', function ($expression) {
        return "<?php } ?>";
    });

});
?>
